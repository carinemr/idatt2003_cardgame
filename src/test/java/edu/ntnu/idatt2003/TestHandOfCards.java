package edu.ntnu.idatt2003;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class TestHandOfCards {
  DeckOfCards deck;
  HandOfCards hand;
  PlayingCard card;

  @BeforeEach
  public void initialize() {
    deck = new DeckOfCards();
    hand = new HandOfCards();

    card = new PlayingCard('H',5);
    //deck.dealHand(4);
    hand.addCardToHand(card);
  }

  @Test
  @DisplayName("AddCardToHand with valid input, card added")
  public void testAddCardToHandPos() {
    PlayingCard newCard = new PlayingCard('S', 7);

    hand.addCardToHand(newCard);

    assertEquals(newCard, hand.getHand().getLast());
  }

  @Test
  @DisplayName("AddCardToHand with invalid input, card already on hand so not added")
  public void testAddCardToHandNeg() {
    int sizeBefore = hand.getHandAmount();

    hand.addCardToHand(card);

    assertEquals(sizeBefore, hand.getHandAmount());
  }
}