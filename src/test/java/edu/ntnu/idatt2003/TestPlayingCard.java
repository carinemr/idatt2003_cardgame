package edu.ntnu.idatt2003;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
public class TestPlayingCard {
  PlayingCard card;
  char HEARTS = 'H';
  char SPADES = 'S';
  char DIAMONDS = 'D';
  char CLUBS = 'C';

  @BeforeEach
  public void initialCard() {
    card = new PlayingCard(HEARTS, 3);
  }

  @Test
  @DisplayName("Test constuctor with positive values, does not throw exceptions")
  public void testConstructorPos() {
    int spadesFace = 6;
    int clubsFace = 13;

    PlayingCard spadesCard = new PlayingCard(SPADES, spadesFace);
    PlayingCard clubsCard = new PlayingCard(CLUBS, clubsFace);

    assertEquals(spadesCard.getSuit(), SPADES);
    assertEquals(spadesCard.getFace(), spadesFace);
    assertEquals(clubsCard.getSuit(), CLUBS);
    assertEquals(clubsCard.getFace(), clubsFace);
  }

  @Test
  @DisplayName("Test constuctor with negative values, throws exceptions")
  public void testConstructorNeg() {
    int invalidHeartsFace = -2;
    int invalidDiamondFace = 14;
    char invalidDiamond = 'I';

    assertThrows(IllegalArgumentException.class, () -> new PlayingCard(HEARTS, invalidHeartsFace));
    assertThrows(IllegalArgumentException.class, () -> new PlayingCard(invalidDiamond, invalidDiamondFace));
  }

  @Test
  @DisplayName("GetAsString returns string")
  public void testGetAsStringReturnsString() {
    String expectedOutput = "H3";

    assertEquals(card.getAsString(), expectedOutput);
  }

  @Test
  @DisplayName("Equals with positive input, does not throw exceptions")
  public void testEqualsPos() {
    int testFace1 = 3;
    int testFace2 = 13;

    PlayingCard testCard1 = new PlayingCard(HEARTS, testFace1);
    PlayingCard testCard2 = new PlayingCard(SPADES, testFace2);

    assertTrue(card.equals(testCard1));
    assertFalse(card.equals(testCard2));
  }
}
