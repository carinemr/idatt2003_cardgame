package edu.ntnu.idatt2003;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
public class TestDeckOfCards {
  DeckOfCards deck;

  @BeforeEach
  public void initialDeck() {
    deck = new DeckOfCards();
  }

  @Test
  @DisplayName("Test constructor and deckCardAmount, 52 card created and verified")
  public void testConstructorValidOutput() {
    int expectedCardAmount = 52;
    int actualDeckCardAmount = deck.deckCardAmount();

    assertEquals(expectedCardAmount, actualDeckCardAmount);
  }

  @Test
  @DisplayName("DealHand with valid input and output is random, does not throw exceptions")
  public void testDealHandPos() {
    int validAmount = 5;

    HandOfCards handDealt1 = deck.dealHand(validAmount);
    HandOfCards handDealt2 = deck.dealHand(validAmount);

    assertNotEquals(handDealt1, handDealt2);
  }

  @Test
  @DisplayName("DealHand with invalid input, throws exceptions")
  public void testDealHandNeg() {
    int invalidAmount1 = -3;
    int invalidAmount2 = 54;

    assertThrows(IllegalArgumentException.class, () -> deck.dealHand(invalidAmount1));
    assertThrows(IllegalArgumentException.class, () -> deck.dealHand(invalidAmount2));
  }
}
