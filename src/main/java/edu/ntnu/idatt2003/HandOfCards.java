package edu.ntnu.idatt2003;

import java.util.ArrayList;
import java.util.List;

public class HandOfCards {
  private final List<PlayingCard> hand;

  public HandOfCards() {
    this.hand = new ArrayList<>();
  }

  public List<PlayingCard> getHand() {
    return hand;
  }

  public int getHandAmount() {
    return hand.size();
  }

  public void addCardToHand(PlayingCard card) {
    if (!hand.contains(card)) {
      hand.add(card);
    }
  }

  public int sumOfHand() {
    return hand.stream()
        .mapToInt(PlayingCard::getFace)
        .sum();
  }

  public List<PlayingCard> cardsOfHeartsOnHand() {
    return hand.stream()
        .filter(card -> card.getSuit() == 'H')
        .toList();
  }

  public boolean queenOfSpadesOnHand() {
    Object queen = new PlayingCard('S', 12);

    return hand.stream()
        .anyMatch(card -> card.equals(queen));
  }

  public boolean flushOnHand() {
    long spades = hand.stream().filter(card -> card.getSuit() == 'S').count();
    long hearts = hand.stream().filter(card -> card.getSuit() == 'H').count();
    long diamonds = hand.stream().filter(card -> card.getSuit() == 'D').count();
    long clubs = hand.stream().filter(card -> card.getSuit() == 'C').count();

    return spades >= 5 || hearts >= 5 || diamonds >= 5 || clubs >= 5;
  }
}
