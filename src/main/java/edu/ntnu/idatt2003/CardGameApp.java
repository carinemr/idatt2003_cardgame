package edu.ntnu.idatt2003;

import java.util.ArrayList;
import java.util.List;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;

import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class CardGameApp extends Application {
  CardGameController controller;
  VBox leftCardBox;
  VBox rightButtonBox;
  VBox cardInventoryBox;
  Text textCardDisplay;

  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage stage) throws Exception {
    HBox root = new HBox();

    leftCardBox = createLeftCardBox();
    leftCardBox.setAlignment(Pos.CENTER);
    rightButtonBox = createRightButtonBox();
    rightButtonBox.setAlignment(Pos.CENTER);

    root.setSpacing(65);

    root.getChildren().addAll(leftCardBox, rightButtonBox);

    root.setAlignment(Pos.CENTER);

    Scene scene = new Scene(root, 700, 600);

    stage.setTitle("CardGame");
    stage.setScene(scene);
    stage.show();
  }

  @Override
  public void init() throws Exception {
    super.init();
    controller = new CardGameController(this);
    leftCardBox = new VBox();
    rightButtonBox = new VBox();

    textCardDisplay = new Text();
  }

  @Override
  public void stop() throws Exception {
    super.stop();
  }

  public VBox createLeftCardBox() {
    cardInventoryBox = new VBox();
    textCardDisplay = new Text("Please click on the deal cards button. Cards will show up here after being dealt.");

    leftCardBox.getChildren().addAll(textCardDisplay, cardInventoryBox);

    return leftCardBox;
  }

  public VBox createRightButtonBox() {
    VBox buttonBox = new VBox(25);

    Button btnDealHand = new Button("Deal hand");
    Button btnCheckHand = new Button("Check hand");

    btnDealHand.setOnAction(actionEvent -> inputRequestDisplay());
    btnCheckHand.setOnAction(actionEvent -> controller.checkHand());

    buttonBox.getChildren().addAll(btnDealHand, btnCheckHand);

    return buttonBox;
  }


  public void inputRequestDisplay() {
    Stage popUp = new Stage();
    popUp.initModality(Modality.APPLICATION_MODAL);

    Text requestMessage = new Text("Please write amount of cards to deal");
    TextField inAmountCards = new TextField();
    inAmountCards.setMaxWidth(45);

    Button btnDeal = new Button("DEAL");
    Button btnGoBack = new Button("GO BACK");

    HBox containerBtn = new HBox(btnDeal, btnGoBack);
    containerBtn.setAlignment(Pos.CENTER);

    btnDeal.setOnAction(actionEvent -> {
          controller.dealHand(inAmountCards.getText());
          popUp.close();
        } );
    btnGoBack.setOnAction(actionEvent -> popUp.close());

    VBox showBox = new VBox(10);
    showBox.getChildren().addAll(requestMessage, inAmountCards, containerBtn);
    showBox.setAlignment(Pos.CENTER);

    Scene popupScene = new Scene(showBox, 400, 200);

    popUp.setScene(popupScene);
    popUp.showAndWait();
  }

  public void showDealtHand(HandOfCards hand) {
    StringBuilder cardString = new StringBuilder();
    hand.getHand().forEach(card -> cardString.append(card.getAsString()).append(" "));
    textCardDisplay.setText(String.valueOf(cardString));
  }

  public void displayCheckHand(String sum, List<PlayingCard> heartCards, String queenSpadesOnHand, String flush) {
    cardInventoryBox.getChildren().clear();

    Text textSum = new Text("\nSum of the faces: ");
    Text textHeartCards = new Text("\nCards of hearts: ");
    Text textFlush = new Text("\nFlush: ");
    Text textQueenSpades = new Text("\nQueen of spades: ");

    Text textDealtSum = new Text(sum);
    Text textIsFlush = new Text(flush);
    Text textQueenSpadesPresent = new Text(queenSpadesOnHand);

    Text textAmountHeartCardsDealt;
    if (heartCards.isEmpty()) {
      textAmountHeartCardsDealt = new Text("No hearts");
    } else {
      StringBuilder heartCardString = new StringBuilder();
      heartCards.forEach(card -> heartCardString.append(card.getAsString()).append(" "));
      textAmountHeartCardsDealt = new Text(String.valueOf(heartCardString));
    }

    cardInventoryBox.getChildren().addAll(textSum,textDealtSum, textHeartCards, textAmountHeartCardsDealt, textFlush, textIsFlush, textQueenSpades, textQueenSpadesPresent);
  }

  public void displayErrorMsg(String errorMsg) {
    Alert alert = new Alert(Alert.AlertType.ERROR);
    alert.setTitle("Error dialog");
    alert.setHeaderText("An error occured.");
    alert.setContentText(errorMsg);
    alert.showAndWait();
  }

}
