package edu.ntnu.idatt2003;

import java.util.List;

public class CardGameController {
  DeckOfCards deck;
  HandOfCards hand;
  CardGameApp view;

  public CardGameController(CardGameApp view) {
    deck = new DeckOfCards();
    hand = new HandOfCards();
    this.view = view;
  }

  public void dealHand(String input) {
    try {
      int n = Integer.parseInt(input);

      try {
        hand = deck.dealHand(n);
        view.showDealtHand(hand);
      } catch (Exception e) {
        String errorMsg = "Number is not between 1 and 52, please try again!";
        view.displayErrorMsg(errorMsg);
      }
    } catch (Exception e) {
      String errorMsg = "Please make sure to input a number!";
      view.displayErrorMsg(errorMsg);
    }
  }

  public void checkHand() {
    if (hand.getHand().isEmpty()) {
      view.displayErrorMsg("Hand is empty. Please use the deal hand button first.");
    } else {
      String sum = String.valueOf(hand.sumOfHand());
      List<PlayingCard> heartCards = hand.cardsOfHeartsOnHand();
      String queenSpadesOnHand;
      String flush;

      if (hand.queenOfSpadesOnHand()) {
        queenSpadesOnHand = "Yes";
      } else {
        queenSpadesOnHand = "No";
      }

      if (hand.flushOnHand()) {
        flush = "Yes";
      } else {
        flush = "No";
      }
      view.displayCheckHand(sum, heartCards, queenSpadesOnHand, flush);
    }
  }
}
