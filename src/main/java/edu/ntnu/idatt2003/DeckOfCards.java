package edu.ntnu.idatt2003;

import java.util.ArrayList;
import java.util.Random;

public class DeckOfCards {
  private final ArrayList<PlayingCard> deck;
  private final char[] suit = { 'S', 'H', 'D', 'C' };
  Random random = new Random();


  /**
   * Creates the deck of cards, where cards from 1 to 13 are created, for all four suits.
   */
  public DeckOfCards() {
    this.deck = new ArrayList<>();
    for (int i = 0; i < suit.length; i++) {
      for (int j = 1; j <= 13; j++) {
        deck.add(new PlayingCard(suit[i], j));
      }
    }
  }

  public int deckCardAmount() {
    return deck.size();
  }

  public HandOfCards dealHand(int n) {
    if (n < 1 || n > 52) {
      throw new IllegalArgumentException("Amount of card dealt need to be between 1 and 52.");
    }

    HandOfCards hand = new HandOfCards();

    while (hand.getHandAmount() < n) {
      int randomCard = random.nextInt(deck.size() );
      hand.addCardToHand(deck.get(randomCard));
    }

    return hand;
  }
}
